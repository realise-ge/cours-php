<?php

class Arme{

    // Attributs de la classe
    private $nbCoups;
    private $puissance;

    // Constructeur de la classe
    public function __construct(){
        // Initialisation des attributs
        $this->nbCoups = 0;
        $this->puissance = 2;
    }

    // Getters
    public function getNbCoups(){
        return $this->nbCoups;
    }

    public function getPuissance(){
        return $this->puissance;
    }

    // Setter
    public function setPuissance($puissance){
        $this->puissance = $puissance;
    }

    // Autre fonction pour modifier les attributs
    public function utiliser(){
        $this->nbCoups++;
    }
}



?>