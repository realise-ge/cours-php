<?php
// import de la classe arme
require_once "arme.php";

class Arc extends Arme{
    // Déclaration des attributs de la classe
    private $nbFleches;

    // Constructeur de la classe
    public function __construct(){
        // Appel au constructeur de la classe mère
        parent::__construct();
        // Initialisation de l'attribut
        $this->nbFleches = 10;
    }

    // Getter de l'attribut
    public function getNbFleches(){
        return $this->nbFleches;
    }

    // Autres fonctions pour modifier l'attribut
    public function ajouterFleches($nbFleches){
        $this->nbFleches += $nbFleches;
    }

    public function tirer(){
        $this->nbFleches--;
    }
}

?>