<?php

/**
 * Ce code déroule une histoire
 * pour l'exécuter mettez-vous dans le dossier 1_cours dans le terminal et exécuter la commande :
 *  php 1_histoire.php
 */

require_once "guerrier.php";
require_once "arc.php";

$jack = new Guerrier();
$arcDeJack = new Arc();

$jack->setArme($arcDeJack);

$jack->sauter();
$jack->courir();
$jack->combattre();
$jack->getArme()->utiliser();

echo "Nombre de coups de l'arme de Jack : ".$jack->getArme()->getNbCoups()."\n";

?>