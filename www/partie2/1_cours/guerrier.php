<?php
// Import de la classe Humain
require_once "humain.php";

// Classe qui étend la classe Humain
class Guerrier extends Humain{

    // Attribut de la classe
    private $arme;

    // Quand pas de constructeur, constructeur implicite par défaut

    // Getter
    public function getArme(){
        return $this->arme;
    }

    // Setter
    public function setArme($arme){
        $this->arme = $arme;
    }

    // Autres fonctions de la classe
    public function combattre(){
        echo "Je me bats\n";
    }

    public function courir(){
        echo "Je ne peux pas courir\n";
    }
}


?>