<?php

class Humain{

    // Attributs de la classe
    private $tete = 1;
    private $bras = 2;
    private $jambes = 2;

    // Quand pas de constructeur, constructeur implicite par défaut

    // Autres fonctions de la classe
    public function marcher(){
        echo "Je marche\n";
    }

    public function courir(){
        echo "Je cours\n";
    }

    public function sauter(){
        echo "Je saute\n";
    }
}

?>