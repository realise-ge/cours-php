<?php

/**
 * Ce fichier PHP est invisible du client.
 * Il joue le rôle de ce qu'on appelle un contrôleur, il contient
 * la logique de l'application qui travaille sur le serveur.
 * 
 * Ici on reçoit les données du formulaire d'ajout de CF
 * et on utilise ses données pour ajouter le CF dans la BD.
 * 
 */

 // Récupération des données du formulaire
$prenom = $_POST["prenom"];
$nom = $_POST["nom"];
$hobby = $_POST["hobby"];

// Utilisation de la classe database pour insérer le nouveau CF reçu
require_once "database.php";
$database = new Database();
$nouvelId = $database->insertCf($prenom, $nom, $hobby);

// Je retourne à la liste des CFs
header("location: vue-classe.php");






?>