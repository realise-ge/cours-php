<?php

/**
 * Ce fichier contient la classe Cf
 */

class Cf {
    // Les attributs de la classe sont les mêmes que les noms des 
    // colonnes dans la base de données 
    private $id;
    private $nom;
    private $prenom;
    private $hobby;

    // On n'a pas besoin de constructeur donc on laisse celui par défaut

    // On implémente les getters et les setters pour chaque attribut
    // Cela va servir à lire et modifier les valeurs des attributs

    public function getId(){ return $this->id; }
    public function getNom(){ return $this->nom; }
    public function getPrenom(){ return $this->prenom; }
    public function getHobby(){ return $this->hobby; }

    // Pas besoin de setter pour l'id car il est géré par la BD
    public function setNom($nom){ $this->nom = $nom; }
    public function setPrenom($prenom){ $this->prenom = $prenom; }
    public function setHobby($hobby){ $this->hobby = $hobby; }
}
