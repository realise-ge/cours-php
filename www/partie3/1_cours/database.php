<?php

require_once "cf.php";

/**
 * Classe qui fait l'interface entre la vue et la base de données
 */

class Database {
    // Attribut privé qui va contenir la connexion à la BD
    private $connexion;

    // On crée la connexion dans le constructeur
    public function __construct(){
        // On stocke les valeurs dont on a besoin pour 
        // faire la connexion dans des variables
        $_HOST = "mariadb";
        $_DATABASE_NAME = "classe";
        $_USER = "adminClasse";
        $_PASSWORD = "P@ssw0rd";

        // On instancie un nouvel objet PDO
        $this->connexion = new PDO("mysql:host=".$_HOST.";dbname=".$_DATABASE_NAME.";",
                                    $_USER,
                                    $_PASSWORD);
        
    }

    // Getter pour pouvoir tester ce que contient la variable connexion
    public function getConnexion(){
        return $this->connexion;
    }

    /**
     * Fonctions pour modifier ou lire la base de données
     */

     /**
      * Fonctions qui lit en base de données tous les Cfs
      * et les retournes sous forme d'un tableau d'objets
      *
      * @return {array} tous les Cfs de la classe
      */
    public function getAllCfs(){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM cfs"
        );
        // Etape 2 : exécution de la requête (sans paramètres)
        $pdoStatement->execute();
        // debug : est ce que la requête s'est bien passées ? (décommenter pour tester)
        //var_dump($pdoStatement->errorInfo());
        // Etape 3 : récupération du résultat sous forme de tableau d'objets
        $tabCfs = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Cf");
        // Etape 4 : on renvoie le résultat
        return $tabCfs;
    }

    /**
     * Fonction qui cherche un CF dans la base de données
     * et le retourne sous forme d'un objet
     *
     * @param [integer] $id
     * @return {Cf} le CF trouvé
     */
    public function getCfById($id){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM cfs WHERE id = :idCf"
        );
        // Etape 2 : exécution de la requête (sans paramètres)
        $pdoStatement->execute([
            "idCf" => $id
        ]);
        // debug : est ce que la requête s'est bien passées ? (décommenter pour tester)
        //var_dump($pdoStatement->errorInfo());
        // Etape 3 : récupération du résultat sous forme d'un objet
        $cf = $pdoStatement->fetchObject("Cf");
        // Etape 4 : on renvoie le résultat
        return $cf;
    }


}