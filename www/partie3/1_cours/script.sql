-- Création de la base de données
CREATE DATABASE classe;

-- Utilisons cette nouvelle base de données
USE classe;

-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminClasse'@'%' IDENTIFIED BY 'P@ssw0rd';
GRANT ALL PRIVILEGES ON classe.* TO 'adminClasse'@'%';

-- Création de la table cfs
CREATE TABLE cfs (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255),
    prenom VARCHAR(255),
    hobby VARCHAR(255)
);

-- Insertion de quelques cfs pour les tests
INSERT INTO cfs (nom, prenom, hobby) VALUES ('Dupond', 'Toto', 'Les promenades');
INSERT INTO cfs (nom, prenom, hobby) VALUES ('Durand', 'Tata', 'Les chevaux');
INSERT INTO cfs (nom, prenom, hobby) VALUES ('Norris', 'Chuck', 'La bagarre');

