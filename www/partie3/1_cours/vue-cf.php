<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="">

    <title>PHP Classe</title>
</head>

<body>
    <?php
        // On importe la classe Database
        require_once "database.php";

        // On instancie un nouvel objet Database ce qui va créer une connexion à la BD
        $database = new Database();

        // On a besoin de l'id du cf (il a été envoyé dans l'url)
        $idCf = $_GET["id"];

        // On doit de nouveau récupérer les info du CF dans la BD
        $cf = $database->getCfById($idCf);

    ?>

    <div class="container">
        <h1 class="text-center">#<?php echo $cf->getId(); ?></h1>
        <div class="row">

            <div class="offset-2 col-8 border border-info">
                <p><?php echo $cf->getNom()." ".$cf->getPrenom(); ?></p>
                <p><?php echo "Aime : ".$cf->getHobby(); ?></p>
            </div>

        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>