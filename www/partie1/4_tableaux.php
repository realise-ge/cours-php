<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 4_tableaux.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Tableaux extends TestCase{

    // Mettez dans resultat la valeur à l'index 1 du tableau a
    public function testExercice1(){
        $a = ["a", "b", "c"];
        $resultat = null;
        $this->assertEquals($a[1], $resultat);
    }

    // Mettez dans index le numéro de l'index de la valeur "a" du tableau
    public function testExercice2(){
        $a = ["a", "b", "c"];
        $index = null;
        $this->assertEquals("a", $a[$index]);
    }

    // Mettez dans index la clé de la valeur "Bob" du tableau
    public function testExercice3(){
        $a = ["a" => "Julie", "b" => "Marc", "c" => "Bob"];
        $index = null;
        $this->assertEquals("Bob", $a[$index]);
    }

    // Mettez dans resultat la valeur pour la clé "a" du tableau
    public function testExercice4(){
        $a = ["a" => "Julie", "b" => "Marc", "c" => "Bob"];
        $resultat = null;
        $this->assertEquals($a["a"], $resultat);
    }

    // Dans a créez un tableau de 5 éléments
    public function testExercice5(){
        $a = null;

        $this->assertEquals(5, count($a));
    }

    // Créez un tableau dans a avec "Je" à l'index 0, "suis" à l'index 1,
    // "un" à l'index 2, "tableau" à l'index 3
    public function testExercice6(){
        $a = null;

        $this->assertEquals(4, count($a));
        $this->assertEquals("Je", $a[0]);
        $this->assertEquals("suis", $a[1]);
        $this->assertEquals("un", $a[2]);
        $this->assertEquals("tableau", $a[3]);
    }

    // Créez un tableau dans a avec "Bulldog" à l'index "chien", "Pinson" à l'index "oiseau",
    // "Persan" à l'index "chat"
    public function testExercice7(){
        $a = null;

        $this->assertEquals(3, count($a));
        $this->assertEquals("Bulldog", $a["chien"]);
        $this->assertEquals("Pinson", $a["oiseau"]);
        $this->assertEquals("Persan", $a["chat"]);
    }

    // Mettez dans la variable resultat le nombre d'élément du tableau a
    // Utilisez la fonction count : https://www.php.net/manual/fr/function.count.php 
    public function testExercice8(){
        $a = ["i1" => "val1", "i2" => "val2", "i3" => "val3", "i4" => "val4"];
        $resultat = null;
        $this->assertEquals(4, $resultat);
    }

}