<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 6_fonctions.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Fonctions extends TestCase{

    // fonction qui additionne les 2 nombres passés en paramètre
    public function add($x, $y){
        return $x + $y;
    }

    // Appelez la fonction add avec 2 chiffres dont la somme vaut 10
    // et stockez le résultat dans la variable resultat
    public function testExercice1(){
        $resultat = null;
        $this->assertEquals(10, $resultat);
    }

    // Initialisez a et b avec deux valeurs de sorte que resultat vale 20
    public function testExercice2(){
        $a = null;
        $b = null;
        $resultat = $this->add($a, $b);
        $this->assertEquals(20, $resultat);
    }

    // Initialisez a et b avec deux valeurs de sorte que resultat vale 15
    public function testExercice3(){
        $a = null;
        $b = null;
        $resultat = $this->add($a, $b) + 5;
        $this->assertEquals(15, $resultat);
    }

    // Initialisez a avec une valeur de sorte que resultat vale 20
    public function testExercice4(){
        $a = null;
        $resultat = $this->add($a, 7);
        $this->assertEquals(20, $resultat);
    }

    // Initialisez a et b avec deux valeurs de sorte que resultat vale 15
    public function testExercice5(){
        $a = null;
        $b = null;
        $resultat = $this->add($this->add($a, $b), 5);
        $this->assertEquals(15, $resultat);
    }

    // Ecrivez le corps de cette fonction
    public function multiplier($f, $g){

    }

    // Ecrivez la fonction multiplier de sorte quelle retourne 
    // le résultat de la multiplication de 2 nombres
    public function testExercice6(){
        $a = 3;
        $b = 2;
        $resultat = $this->multiplier($a, $b);
        $this->assertEquals(6, $resultat);
    }

    // Ecrivez la fonction saluer ci-dessous

    // Implémentez la fonction saluer pour que resultat contienne "Salut Toto"
    public function testExercice7(){
        $nom = "Toto";
        $resultat = $this->saluer($nom);
        $this->assertEquals("Salut Toto", $resultat);
    }

    // Fonction à utiliser
    public function accord($param){
        if($param < 10){
            return false;
        }
        return true;
    }

    // Donnez une valeur à age afin que resultat contienne true
    public function testExercice8(){
        $age = null;
        $resultat = $this->accord($age);
        $this->assertTrue($resultat);
    }

    // Donnez une valeur à age afin que resultat contienne false
    public function testExercice9(){
        $age = null;
        $resultat = $this->accord($age);
        $this->assertFalse($resultat);
    }


}