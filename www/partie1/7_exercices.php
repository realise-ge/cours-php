<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 7_exercices.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Exercices extends TestCase{


    // Ecrire une fonction qui prend 2 nombres en entrée et retourne le résultat de leur multiplication
    // Utilisez les tests implémenté ci-dessous pour tester votre fonction
    public function testExercice1(){
        $a = 2;
        $b = 3;
        $resultat = null; // remplacer par l'appel de votre fonction
        $this->assertEquals(6, $resultat);

        $a = 0;
        $b = 6;
        $resultat = null;
        $this->assertEquals(0, $resultat);

        $a = 10;
        $resultat = null; // passez a en 1er et 2d argument de votre fonction
        $this->assertEquals(100, $resultat);

    }



    // Ecrire une fonction qui prend 2 nombres en entrée et retourne le plus grand
    // Utilisez les tests implémenté ci-dessous pour tester votre fonction
    public function testExercice2(){
        $a = 10;
        $b = 15;
        $resultat = null;
        $this->assertEquals($b, $resultat);

        $a = 5;
        $b = 3;
        $resultat = null;
        $this->assertEquals($a, $resultat);

        $a = 6;
        $b = 6;
        $resultat = null;
        $this->assertEquals($a, $resultat);
    }

    // Ecrire une fonction qui retourne le nombre d'espace contenu dans une phrase
    // Utilisez les tests implémenté ci-dessous pour tester votre fonction
    public function testExercice3(){
        $phrase = "toto part en bus !";
        $nbSpaces = null; // remplacer par l'appel de votre fonction
        $this->assertEquals(4, $nbSpaces);
    }


    // Ecrire une fonction qui inverse les caractères d'un mot
    public function testExercice4(){
        $mot = "vache";
        $resultat = null; // remplacer par l'appel de votre fonction
        $this->assertEquals("ehcav", $resultat);

        $mot = "non";
        $resultat = null; // remplacer par l'appel de votre fonction
        $this->assertEquals("non", $resultat);
    }

    // Ecrire une fonction qui prend en argument un tableau de mots
    // et retourne la concaténation de tous ces mots
    // ex: ["toto", "tata", "titi"] => retourne "tototatatiti"
    public function testExercice5(){
        $tab = ["toto", "tata", "titi"];
        $resultat = null; // remplacer par l'appel de votre fonction
        $this->assertEquals("tototatatiti", $resultat);
    }

    // Ecrire une fonction qui prend un tableau de mots en argument
    // et retourne le plus grand mot
    public function testExercice6(){
        $tab = ["un", "deux", "trois", "quatre", "cinq"];
        $resultat = null;
        $this->assertEquals("quatre", $resultat);
    }

    // Fonction à compléter
    // Si le perroquet parle entre 22h et 7h du matin on doit retourner "Le perroquet nous perturbe"
    // S'il parle entre 8h et 21h ou s'il ne parle pas, il ne nous dérange pas. Retourner "Tout va bien"
    public function perroquet($parle, $heure){

        return null;
    }

    // Complétez la fonction ci-dessus. le paramètre parle est un booléen (true ou false)
    // S'il vaut true le perroquet est en train de parler, s'il vaut false le perroquet est silencieux
    // Le paramètre heure peut prendre un chiffre en 0 et 23 qui représente l'heure de la journée
    public function testExercice7(){
        $resultat = $this->perroquet(true, 23);
        $this->assertEquals("Le perroquet nous perturbe", $resultat);

        $resultat = $this->perroquet(true, 5);
        $this->assertEquals("Le perroquet nous perturbe", $resultat);

        $resultat = $this->perroquet(false, 23);
        $this->assertEquals("Tout va bien", $resultat);

        $resultat = $this->perroquet(false, 4);
        $this->assertEquals("Tout va bien", $resultat);

        $resultat = $this->perroquet(true, 7);
        $this->assertEquals("Le perroquet nous perturbe", $resultat);

        $resultat = $this->perroquet(true, 12);
        $this->assertEquals("Tout va bien", $resultat);
    }

    ///////////////////////////////////////////////////////////////////

    // Fonction pour calculer la moyenne
    public function moyenne($notes){
        return null;
    }

    // Fonction qui donne le nombre de notes au-dessus ou égales à 10
    public function nbNotesSup($notes){
        return null;
    }

    // Fonction qui donne le nombre de notes en dessous de la moyenne
    public function nbNoteInf($notes, $moyenne){
        return null;
    }

    // Fonction qui donne le nom du meilleur élève
    public function meilleur($notes){
        return null;
    }

    // Fonction qui donne le nom du moins bon élève
    public function cancre($notes){
        return null;
    }

    // Impléementez toutes les fonction ci-dessus pour que tous les tests 
    // ci-dessous puissent passer
    public function testExercice8(){
        $notes = [
            "Pierre" => 18,
            "Paul"   => 14,
            "Jacques" => 7,
            "Sylvie A." => 16,
            "Emmanuelle" => 14,
            "Michael" => 12,
            "Emma" => 6,
            "Yohan" => 5,
            "Flora" => 19,
            "Justine" => 10,
            "Sylvie B." => 10,
            "Augustin" => 16   
        ];

        $moyenneClasse = $this->moyenne($notes);
        $this->assertEquals(12.5, $moyenneClasse);

        $nbNoteSup = $this->nbNotesSup($notes);
        $this->assertEquals(9, $nbNoteSup);

        $nbNotesInf = $this->nbNoteInf($notes, $moyenneClasse);
        $this->assertEquals(4, $nbNotesInf);

        $meilleur = $this->meilleur($notes);
        $this->assertEquals("Flora", $meilleur);

        $cancre = $this->cancre($notes);
        $this->assertEquals("Yohan", $cancre);
    }


}