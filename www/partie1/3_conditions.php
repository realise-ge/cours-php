<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 3_conditions.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Conditions extends TestCase{

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice1(){
        $a = null;
        if(3 < 5){
            $a = 5;
        }else{
            $a = 3;
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice2(){
        $a = null;
        if(15 < 12){
            $a = "Do";
        }else{
            $a = "Ré";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice3(){
        $a = null;
        if(12 <= 12){
            $a = true;
        }else{
            $a = false;
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice4(){
        $a = null;
        if(15 > 12){
            $a = 3;
        }else{
            $a = 10;
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice5(){
        $a = null;
        if(2 > 5){
            $a = "Toto";
        }else{
            $a = "Dupond";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice6(){
        $a = null;
        if(4 >= 3){
            $a = false;
        }else{
            $a = true;
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice7(){
        $a = null;
        if(15 === 12){
            $a = "15";
        }else{
            $a = "12";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice8(){
        $a = null;
        if(5 === 5){
            $a = 5;
        }else{
            $a = "Toto";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice9(){
        $a = null;
        if(5 === "5"){
            $a = 5;
        }else{
            $a = "Toto";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de a après le if dans la variable resultat
    public function testExercice10(){
        $a = null;
        if(5 == "5"){
            $a = 5;
        }else{
            $a = "Toto";
        }
        $resultat = null;
        $this->assertEquals($resultat, $a);
    }

    // Ecrivez la valeur de b pour que a soit true
    public function testExercice11(){
        $a = null;
        $b = null;
        if($b < 7){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(true, $a);
    }

    // Ecrivez la valeur de b pour que a soit false
    public function testExercice12(){
        $a = null;
        $b = null;
        if($b < 7){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(false, $a);
    }

    // Ecrivez la valeur de b pour que a soit true
    public function testExercice13(){
        $a = null;
        $b = null;
        if($b > 20){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(true, $a);
    }

    // Ecrivez la valeur de b pour que a soit false
    public function testExercice14(){
        $a = null;
        $b = null;
        if($b > 20){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(false, $a);
    }

    // Ecrivez la valeur de b pour que a soit true
    public function testExercice15(){
        $a = null;
        $b = null;
        if($b == "Toto"){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(true, $a);
    }

    // Ecrivez la valeur de b pour que a soit true
    public function testExercice16(){
        $a = null;
        $b = null;
        if($b != "Toto"){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(true, $a);
    }

    // Ecrivez la valeur de b pour que a soit false
    public function testExercice17(){
        $a = null;
        $b = null;
        if($b != "Oui"){
            $a = true;
        }else{
            $a = false;
        }
        $this->assertEquals(false, $a);
    }
}

?>