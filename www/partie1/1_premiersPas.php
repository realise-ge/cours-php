<?php
/**
 * Ouvrez un terminal et déplacez vous dans le dossier partie1
 * cd partie1
 * 
 * Pour exécuter votre code PHP utilisez la commande php
 * php 1_premiersPas.php
 * 
 * Tip : pour afficher un retour à la ligne utilisez "\n"
 */


// Déclarez une variable nommée maVariable sans lui donner de valeur


// Donnez une valeur numérique (number) à maVariable et affichez sa valeur avec echo
// https://www.php.net/manual/fr/function.echo.php 


// Affichez le type de la variable maVariable
// https://www.php.net/manual/fr/function.gettype.php 


// Donnez une chaine de caractères (string) à maVariable et affichez sa valeur


// Affichez le type de maVariable


// Donnez une valeur booléean (boolean) à maVariable et affichez sa valeur


// Affichez le type de maVariable


?>