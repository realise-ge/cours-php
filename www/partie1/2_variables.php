<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 2_variables.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Variables extends TestCase{

    // Initialisez les variables a et b pour que les 2 prochains tests passent
    public function testExercice1(){
        $a = null;
        $b = null;

        // tester l'addition de a et b
        $this->assertEquals(3, $a + $b);

        // tester la multiplication de a et b
        $this->assertEquals(2, $a * $b);
    }

    // Initialisez c pour que d ait pour valeur 6
    public function testExercice2(){
        $c = null;
        $d = 4 + $c;
        $this->assertEquals(6, $d);
    }

    // Initialisez c pour afficher la valeur 12
    public function testExercice3(){
        $c = null;
        $d = $c * 4;
        $this->assertEquals(12, $d);
    }


    // Initialisez f pour afficher 7
    public function testExercice4(){
        $e = 2;
        $f = null;
        $g = $e + $f;
        $this->assertEquals(7, $g);
    }

    // initialisez nom avec le mot Toto 
    // puis concaténez les variables nom et hello pour former une phrase
    public function testExercice5(){
        $nom = null;
        $hello = "Salut ";
        $phrase = null;
        $this->assertEquals("Salut Toto", $phrase);
    }

    // Ecrivez dans resultat le resultat de la concaténation entre a et b
    public function testExercice6(){
        $a = "rdw";
        $b = "gft";
        $resultat = null;
        $this->assertEquals($resultat, $a.$b);
    }

}

?>