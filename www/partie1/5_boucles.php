<?php

/**
 * Pour tester les exercices et savoir s'ils sont bien résolus
 * lancez le test en utilisant la commande :
 *      ../vendor/bin/phpunit 5_boucles.php
 * Il faut que vous soyiez dans le dossier partie1
 */

// Je vais utiliser cette librairie pour tester le résultat des opérations
use PHPUnit\Framework\TestCase;

class Boucles extends TestCase{

    // Devinez la valeur de a à la fin de la boucle for et mettez là dans resultat
    // Tip : l'opérateur ++ ajoute 1 à la valeur initiale
    public function testExercice1(){
        $a = 1;
        for($i = 0 ; $i < 5 ; $i++){
            $a++;
        }
        $resultat = null;
        $this->assertEquals($a, $resultat);
    }

    // Mettez la bonne valeur dans max pour que a vale 7 à la fin de la boucle
    public function testExercice2(){
        $a = 0;
        $max = null;
        for($i = 0 ; $i < $max ; $i++){
            $a++;
        }
        $this->assertEquals(7, $a);
    }

    // Mettez la bonne valeur dans max et min pour parcourir tout le tableau
    public function testExercice3(){
        $a = ["hier", "aujoud'hui", "demain"];
        $min = null;
        $max = null;
        for($i = $min ; $i < $max ; $i++){
            // Affiche l'élément du tableau à l'index i
            echo $a[$i];
        }
        $this->assertEquals(0 , $min);
        $this->assertEquals(count($a), $max);
    }

    // Remplacez la valeur null par une valeur ou variable adéquoite
    // pour remplir le tableau par les valeurs de 0 à 99
    // ex: [0 => 0, 1 => 1, ..... 99 => 99]
    public function testExercice4(){
        $a = [];
        for($i = 0 ; $i < 100 ; $i++){
            // Affectation d'une valeur dans a à l'index i
            $a[$i] = null;
        }
        $this->assertEquals(0, $a[0]);
        $this->assertEquals(99, $a[99]);
    }

    // Etudiez la boucle foreach et devinez quelle sera la valeur de resultat
    // à la fin de la boucle. Mettez votre valeur dans guess
    public function testExercice5(){
        $resultat = 0;
        $a = [3, 4, 6];
        foreach($a as $val){
            $resultat = $resultat + $val;
        }
        $guess = null;
        $this->assertEquals($resultat, $guess);
    }

    // Utilisez une boucle pour remplir le tableau b
    // avec les valeurs du tableau a
    public function testExercice6(){
        $a = ["Toto", "Tata", "Tutu"];
        $b = [];
        // Ecrivez dessous votre boucle


        $this->assertEquals($a, $b);

    }

    // Instanciez un tableau a avec les bonnes valeurs (au moins 3) pour
    // que le resultat vale 10
    public function testExercice7(){
        $a = [];
        $resultat = 0;
        foreach($a as $chiffre){
            // Equivaut à $resultat = $resultat + $chiffre
            $resultat += $chiffre;
        }
        $this->assertEquals(10, $resultat);
    }

    // Dans guess devinez la valeur de resultat à la fin de la boucle while
    public function testExercice8(){
        $resultat = 0;
        while($resultat < 10){
            $resultat++;
        }
        $guess = null;
        $this->assertEquals($resultat, $guess);
    }

    // Ecrivez une boucle qui parcours le tableau tant qu'elle n'a pas
    // trouvé le mot "quatre"
    public function testExercice9(){
        $tab = ["un", "deux", "trois", "quatre", "cinq"];
        $i = 0;
        // Ecrivez en dessous une boucle while

        $this->assertEquals("quatre", $tab[$i]);
    }

}